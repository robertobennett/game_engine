#include "stdafx.h"
#include "Renderer.h"

CRenderer::CRenderer() :
m_bInitialized(false)
{
}

CRenderer::~CRenderer()
{
}

bool CRenderer::Init(HWND hWnd)
{
	if (!m_bInitialized)
	{
		m_bInitialized = true;
		return true;
	}

	return false;
}

void CRenderer::Update()
{
}

void CRenderer::Shutdown()
{
}
