#ifndef RENDERER_DLL_RENDERER_H
#define RENDERER_DLL_RENDERER_H

class DLL_IMPORT_EXPORT CRenderer
{
	public:
		CRenderer();
		~CRenderer();

		bool Init(HWND hWnd);
		void Update();
		void Shutdown();

	private:
		bool m_bInitialized;
};

#endif