#ifndef RENDERER_DLL_STDAFX_H
#define RENDERER_DLL_STDAFX_H

#include <Windows.h>

#ifdef RENDERER_DLL_EXPORT
#define DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#endif