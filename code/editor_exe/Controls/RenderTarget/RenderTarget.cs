﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interop;
using System.Windows.Media;

namespace editor_exe.Controls.RenderTarget
{
    public class RenderTargetHost : Border
    {
        private RenderTarget m_RenderTargetWindow = null;
        public RenderTarget RenderTargetWindow
        {
            get
            {
                return m_RenderTargetWindow;
            }
        }
        private Window m_ParentWindow = null;
        public RenderTargetHost()
        {
        }

        public override void EndInit()
        {
            this.SizeChanged += Resized;
            this.Loaded += OnLoad;
            this.MouseEnter += OnMouseEnter;
            this.MouseLeave += OnMouseLeave;

            base.EndInit();
        }

        private void OnMouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
            m_ParentWindow.Focus();
            m_RenderTargetWindow.Focused = false;
        }

        private void OnMouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
            m_RenderTargetWindow.Focused = true;
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
            var parent = VisualTreeHelper.GetParent(this);
            while (!(parent is Window))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            m_ParentWindow = (Window)parent;

            if (m_ParentWindow == null)
                throw new Exception("Parent window passed is null");

            m_RenderTargetWindow = new RenderTarget((int)this.ActualWidth, (int)this.ActualHeight);
            this.Child = m_RenderTargetWindow;
        }

        /// <summary>
        /// Called when the border is resized
        /// </summary>
        /// <param name="sender">The object the event was triggered from ?</param>
        /// <param name="e">The event arguments</param>
        private void Resized(object sender, SizeChangedEventArgs e)
        {
            if (m_RenderTargetWindow != null)
                m_RenderTargetWindow.Resize((int)e.NewSize.Width, (int)e.NewSize.Height);
        }
    }

    public class RenderTarget : HwndHost
    {
        #region Win32
        protected const long WS_CHILD = 0x40000000L, WS_VISIBLE = 0x10000000L, WS_DLGFRAME = 0x00400000L;

        [DllImport("User32.dll")]
        protected extern static IntPtr CreateWindowEx(int dwExStyle, string lpClassName, string lpWindowName, long dwStyle, int iX, int iY, int iWidth, int iHeight, IntPtr hWndParent, IntPtr hMenu, IntPtr hInstance, IntPtr lpParam);
        [DllImport("User32.dll")]
        protected extern static IntPtr DefWindowProc(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);
        [DllImport("User32.dll")]
        protected extern static bool MoveWindow(IntPtr hWnd, int X, int Y, int cx, int cy, bool repaint);
        [DllImport("User32.dll")]
        protected extern static IntPtr SetFocus(IntPtr hWnd);
        /// <summary>
        /// Sets the windows position, size and some flags: https://msdn.microsoft.com/en-us/library/windows/desktop/ms633545(v=vs.85).aspx
        /// </summary>
        /// <param name="hWnd">Target window</param>
        /// <param name="hWndInsertAfter">Z order, 0 is top most</param>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <param name="uFlags">Some flags, 0x0004 - stops Z order from adjusting</param>
        /// <returns></returns>
        [DllImport("User32.dll")]
        protected extern static IntPtr DestroyWindow(IntPtr hWnd);
        #endregion

        #region HwndHost
        protected override HandleRef BuildWindowCore(HandleRef hwndParent)
        {
            m_hWnd = CreateWindowEx(0, "static", ("RenderTarget" + m_uiID), WS_CHILD | WS_VISIBLE | WS_DLGFRAME, 0, 0, m_iWidth, m_iHeight, hwndParent.Handle, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
            if (m_hWnd == IntPtr.Zero)
                throw new Exception("Failed to create window !");

            return new HandleRef(this, m_hWnd);
        }

        protected override IntPtr WndProc(IntPtr hWnd, int iMsg, IntPtr wParam, IntPtr lParam, ref bool bHandled)
        {
            if (m_WndProcListener == null)
            {
                bHandled = false;
            }
            else
            {
                if (m_WndProcListener(hWnd, iMsg, wParam, lParam))
                    bHandled = true;
                else
                    bHandled = false;
            }

            return m_hWnd;
        }

        protected override void DestroyWindowCore(HandleRef hwnd)
        {
            DestroyWindow(m_hWnd);
        }
        #endregion

        public delegate bool WndProcDelegate(IntPtr hWnd, int iMsg, IntPtr wParam, IntPtr lParam);
        private WndProcDelegate m_WndProcListener = null;
        public WndProcDelegate WndProcListener
        {
            get
            {
                return m_WndProcListener;
            }
            set
            {
                m_WndProcListener = value;
            }
        }
        private bool m_bFocused = false;
        public bool Focused
        {
            set
            {
                if (value == true)
                {
                    SetFocus(m_hWnd);
                }

                m_bFocused = value;
            }
            get
            {
                return m_bFocused;
            }
        }

        private int m_iWidth = 0, m_iHeight = 0;
        private uint m_uiID = 0;
        private IntPtr m_hWnd = IntPtr.Zero;
        public IntPtr HWND
        {
            get
            {
                if (m_hWnd == IntPtr.Zero)
                    throw new Exception("RenderTarget" + m_uiID + " is NULL");

                return m_hWnd;
            }
        }

        public RenderTarget(int iWidth, int iHeight)
        {
            m_iWidth = iWidth;
            m_iHeight = iHeight;
            m_uiID = sm_uiNextID;
            sm_uiNextID++;

            if (m_iWidth <= 0 || m_iWidth <= 0)
                throw new Exception("Invalid RenderTarget dimensions (width/height): " + m_iWidth + "/" + m_iWidth);

            return;
        }

        public void Resize(int iWidth, int iHeight)
        {
            m_iWidth = iWidth;
            m_iHeight = iHeight;

            if (m_iWidth <= 0 || m_iWidth <= 0)
                throw new Exception("Invalid RenderTarget dimensions (width/height): " + m_iWidth + "/" + m_iWidth);
        }

        private static uint sm_uiNextID = 1;
    }
}