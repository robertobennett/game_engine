#ifndef EDITOR_DLL_EDITOR_H
#define EDITOR_DLL_EDITOR_H

#include <Engine/Include/Engine.h>

class DLL_IMPORT_EXPORT CEditor
{
	public:
		CEditor();
		~CEditor();

		bool Init(HWND hWnd);
		void Update();
		void Shutdown();

	private:
		bool m_bInitialized;

		CEngine * m_pEngine;
};

#endif