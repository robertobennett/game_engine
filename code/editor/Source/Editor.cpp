#include "stdafx.h"
#include "Editor.h"

CEditor::CEditor() :
m_bInitialized(false),
m_pEngine(nullptr)
{
}

CEditor::~CEditor()
{
}

bool CEditor::Init(HWND hWnd)
{
	if (!m_bInitialized)
	{
		m_pEngine = new CEngine();
		if (!m_pEngine->Init(hWnd))
			return false;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CEditor::Update()
{
}

void CEditor::Shutdown()
{
	if (m_pEngine != nullptr)
	{
		delete m_pEngine;
		m_pEngine = nullptr;
	}
}
