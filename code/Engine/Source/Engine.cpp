#include "stdafx.h"
#include "Engine.h"

CEngine::CEngine() :
m_bInitialized(false),
m_pRenderer(nullptr)
{
}

CEngine::~CEngine()
{
}

bool CEngine::Init(HWND hWnd)
{
	if (!m_bInitialized)
	{
		m_pRenderer = new CRenderer();
		if (!m_pRenderer->Init(hWnd))
			return false;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void CEngine::Update()
{
}

void CEngine::Shutdown()
{
	if (m_pRenderer != nullptr)
	{
		delete m_pRenderer;
		m_pRenderer = nullptr;
	}
}
