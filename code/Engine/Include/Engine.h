#ifndef ENGINE_DLL_ENGINE_H
#define ENGINE_DLL_ENGINE_H

#include <Renderer/Include/Renderer.h>

class DLL_IMPORT_EXPORT CEngine
{
	public:
		CEngine();
		~CEngine();

		bool Init(HWND hWnd);
		void Update();
		void Shutdown();

	private:
		bool m_bInitialized;

		CRenderer * m_pRenderer;
};

#endif