#ifndef EDITOR_WRAPPER_DLL_EDITOR_WRAPPER_H
#define EDITOR_WRAPPER_DLL_EDITOR_WRAPPER_H

#include <editor/Include/Editor.h>

using namespace System;

namespace EditorWrapper 
{
	public ref class CEditorWrapper
	{
		public:
			CEditorWrapper();
			~CEditorWrapper();
			!CEditorWrapper();

			bool Init(IntPtr^ hWnd);
			void Update();
			void Shutdown();

		private:
			bool m_bInitialized;
			CEditor * m_pEditor;
	};
}

#endif