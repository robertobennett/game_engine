#include "stdafx.h"
#include "Editor_Wrapper.h"

#include <msclr\marshal.h>

EditorWrapper::CEditorWrapper::CEditorWrapper() :
m_bInitialized(false),
m_pEditor(nullptr)
{
	throw gcnew System::NotImplementedException();
}

EditorWrapper::CEditorWrapper::~CEditorWrapper()
{
	throw gcnew System::NotImplementedException();
}

EditorWrapper::CEditorWrapper::!CEditorWrapper()
{
	throw gcnew System::NotImplementedException();
}

bool EditorWrapper::CEditorWrapper::Init(IntPtr^ hWnd)
{
	if (!m_bInitialized)
	{
		msclr::interop::marshal_context contx;

		m_pEditor = new CEditor();
		if (!m_pEditor->Init((HWND)(hWnd->ToPointer())))
			return false;

		m_bInitialized = true;
		return true;
	}

	return false;
}

void EditorWrapper::CEditorWrapper::Update()
{
	throw gcnew System::NotImplementedException();
}

void EditorWrapper::CEditorWrapper::Shutdown()
{
	if (m_pEditor != nullptr)
	{
		delete m_pEditor;
		m_pEditor = nullptr;
	}
}
