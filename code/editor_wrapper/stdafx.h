#ifndef EDITOR_WRAPPER_DLL_STDAFX_H
#define EDITOR_WRAPPER_DLL_STDAFX_H

#include <Windows.h>

#ifdef EDITOR_DLL_EXPORT
#define DLL_IMPORT_EXPORT __declspec(dllexport)
#else
#define DLL_IMPORT_EXPORT __declspec(dllimport)
#endif

#endif